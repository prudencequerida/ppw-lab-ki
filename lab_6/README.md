Make Chat Box Page



 Add  lab_6.html into templates folder

 Add lab_6.css into ./static/css


 Add lab_6.js file into lab_6/static/js folder

 Complete the lab_6.js file so it can be run



Implement Calculator



 Add section of code into  lab_6.html file on templates folder

 Add section of code into  lab_6.css file on ./static/css folder

 Add section of code into lab_6.js file on lab_6/static/js folder

 Implement AC feature



Implement select2



 Load theme default match with selectedTheme

 Populate data themes from local storage to select2

 Local storage contains themes and selectedTheme

 Color can change whenever we select selectedTheme



Make sure you have a good Code Coverage



  If you have not done the configuration to show Code Coverage in Gitlab, you must configure it in Show Code Coverage in Gitlab on README.md


 Make sure your Code Coverage is 100%.





Challenge Checklist


Qunit exercise



 Implement from Qunit exercise


Chose one of these exercise you have to complete:


Implement enter button on chat box



 Make a Unit Test using Qunit

 Make a function that make the Unit Test is passed



Implement  sin, log, and tan function (HTML file is available)



 Make a Unit Test using Qunit

 Maka a function that make the Unit Test is passed
