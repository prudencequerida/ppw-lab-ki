from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from http.cookies import SimpleCookie
import environ


root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')
# Create your tests here.

class Lab9UnitTest(TestCase):
    def test_lab_9_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_login_fail(self):
        response = self.client.post('/lab-9/custom_auth/login/',{'username': 'salah', 'password': 'salah'})
        self.assertEqual(response.status_code,302)

    def test_login_success_and_logout(self):
        response = self.client.post('/lab-9/custom_auth/login/',{'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        self.assertEqual(response.status_code,302)
        response = self.client.get('/lab-9/custom_auth/logout/')
        self.assertEqual(response.status_code,302)

    def test_login_session_lab_9(self):
        session = self.client.session
        session['user_login'] = 'wikan.setiaji'
        session['access_token'] = 'something'
        session['kode_identitas'] = 'some code'
        session['role'] = 'mahasiswa'
        session.save()
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code,302)
        self.assertRedirects(response,'/lab-9/profile/',302,200)

    def test_profile_lab_9_not_login(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code,302)
        self.assertRedirects(response,'/lab-9/',302,200)

    def test_profile_lab_9_login_with_drones(self):
        session = self.client.session
        session['user_login'] = 'wikan.setiaji'
        session['access_token'] = 'something'
        session['kode_identitas'] = 'some code'
        session['role'] = 'mahasiswa'
        session['drones'] = ['0000','241410','215151511']
        session.save()
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code,200)

    def test_add_session_drones_no_drones_before(self):
        response = self.client.get('/lab-9/add_session_drones/1/')
        self.assertEqual(response.status_code, 302)

    def test_add_session_drones_some_drones_before(self):
        session = self.client.session
        session['drones'] = ['0000','241410','215151511']
        session.save()
        response = self.client.get('/lab-9/add_session_drones/1/')
        self.assertEqual(response.status_code, 302)

    def test_del_drones(self):
        session = self.client.session
        session['drones'] = ['0000','241410','215151511']
        session.save()
        response = self.client.get('/lab-9/del_session_drones/0000/')
        session= self.client.session
        self.assertNotIn('0000',session['drones'])

    def test_clear_drones(self):
        session = self.client.session
        session['drones'] = ['0000','241410','215151511']
        session.save()
        response = self.client.get('/lab-9/clear_session_drones/')
        session= self.client.session
        self.assertNotIn('drones',session)

    def test_cookie_not_login_yet(self):
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)

    def test_cookie_login(self):
        self.client.cookies = SimpleCookie({'user_login': 'user', 'user_password': 'password',})
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code,302)
        self.assertRedirects(response,'/lab-9/cookie/profile/',302,200)

    def test_cookie_auth_login(self):
        response = Client().post('/lab-9/cookie/auth_login/', {'username' : 'salah', 'password' : 'salah banget'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,'/lab-9/cookie/login/',302,200)

        response = Client().post('/lab-9/cookie/auth_login/', {'username' : 'user', 'password' : 'password'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)


    def test_cookie_profile(self):
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code,302)
        self.assertRedirects(response,'/lab-9/cookie/login/',302,200)

        self.client.cookies = SimpleCookie({'user_login': 'salah', 'user_password': 'salah dongg',})
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'lab_9/cookie/login.html')

        self.client.cookies = SimpleCookie({'user_login': 'user', 'user_password': 'password',})
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

    def test_cookie_clear(self):
        self.client.cookies = SimpleCookie({'user_login': 'usr', 'lang': 'pass',})
        response = self.client.get('/lab-9/cookie/clear/')
        self.assertEqual(response.status_code,302)
        self.assertRedirects(response,'/lab-9/cookie/login/',302,200)
