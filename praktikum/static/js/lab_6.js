///////////////////////////////////////////// Calculator /////////////////////////////////////////////
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
    if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
    } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
    } else if (x === 'log') {
    print.value = Math.log10(print.value);
    erase = true;
    } else if (x === 'sin') {
     print.value = Math.sin(print.value);
     erase = true;
    } else if (x === 'tan') {
     print.value = Math.tan(print.value);
     erase = true;
    } else {
    print.value += x;
    }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

///////////////////////////////////////////// Theme /////////////////////////////////////////////
function applyTheme(theme){
  $('body').css('background', theme.bcgColor);
  $('html').css('color', theme.fontColor);
}

localStorage.themes = `[
{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]`


if (!localStorage.getItem("selectedTheme")) {
  localStorage.selectedTheme = JSON.stringify({"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}});
}
applyTheme(JSON.parse(localStorage.getItem("selectedTheme")))

$(document).ready(function() {
    $('.my-select').select2({
         'data': JSON.parse(localStorage.themes)
       });

    $('.apply-button').on('click', () => {
      var themeid = parseInt($('.my-select').select2('data')[0].id);
      localStorage.selectedTheme = JSON.stringify(JSON.parse(localStorage.themes)[themeid]);
      applyTheme(JSON.parse(localStorage.selectedTheme));
    });

});

